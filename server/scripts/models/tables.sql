--[USER]--
--User Table--
DROP TABLE IF EXISTS users;
CREATE TABLE users (
  id serial NOT NULL PRIMARY KEY,
  username varchar(50) NOT NULL,
  password varchar(255) NOT NULL,
  email varchar(255) NOT NULL,
  contact_number varchar(11) NOT NULL,
  logged_in boolean,
  app_token varchar(255),
  deleted boolean default 'false'
);

--[SERVICEs]--
--Services Table--
CREATE TABLE services (
  id serial primary key,
  owner_id serial references users(id),
  name text Not Null,
  type text Not Null,
  info varchar(1000),
  deleted boolean default 'false'
);


--[EVENTS]--
--Events Table--
DROP TABLE IF EXISTS events;
CREATE TABLE events (
  id serial primary key,
  name text,
  client_id serial references users(id),
  manager_id serial references users(id),
  event_type text,
  due_date varchar(50),
  deleted boolean default 'false'
);

DROP TABLE IF EXISTS prices;
CREATE TABLE prices(
  id serial primary key,
  service_id serial references services(id),
  price int,
  svc_package text,
  hired_by serial references users(id),
  deleted boolean default 'false'
);

DROP TABLE IF EXISTS canvass;
CREATE TABLE canvass(
  id serial primary key,
  event_id serial references events(id),
  service_id serial references services(id),
  package_id serial references prices(id),
  deleted boolean default 'false'
);

--[CONTACT_NUMBER]--
--Contact Number Table--
DROP TABLE IF EXISTS contact_number;
CREATE TABLE contact_number (
  user_id serial references users(id),
  service_id serial references services(id),
  contact_number varchar(11) NOT NULL,
  deleted boolean default 'false'
);

--[ADDRESS]--
--Address Table--
DROP TABLE IF EXISTS essentials;
CREATE TABLE essentials(
  id serial primary key not null,
  event_id serial references events(id),
  essentials text,
  deleted boolean default 'false'
);

DROP TABLE IF EXISTS sponsored;
CREATE TABLE sponsored(
  service_id serial references services(id),
  due_date text,
  sponsor_type text,
  deleted boolean default 'false'
);

DROP TABLE IF EXISTS favorites;
CREATE TABLE favorites(
  service_id serial references services(id),
  user_id serial references users(id),
  deleted boolean default 'false'
);

DROP TABLE IF EXISTS messages;
CREATE TABLE messages(
  id serial primary key,
  sender_id serial references users(id),
  receiver_id serial references users(id),
  msg_content text,
  msg_timestamp text,
  deleted boolean default 'false'
);

DROP TABLE IF EXISTS reviews;
CREATE TABLE reviews(
  id serial primary key,
  user_id serial references users(id),
  service_id serial references services(id),
  rating int,
  review_content text,
  vote int,
  review_timestamp text,
  deleted boolean default 'false'
);

  
--[HIRES]--
--Hires Table--
CREATE TABLE hires (
  id serial primary key,
  usr_id int references users(id),
  svc_id int references users(id),
  start_date date,
  end_date date,
  deleted boolean default 'false'
);

--[EVENT_HIRES]--
--Event_Hires Table--
CREATE TABLE event_hires (
  id serial primary key,
  event_id int references events(id),
  svc_id int references users(id),
  is_hired bool,
  deleted boolean default 'false'
);

CREATE TABLE service_prices (
  id serial references services(id),
  package text Not Null,
  price float,
  deleted boolean default 'false'
);

