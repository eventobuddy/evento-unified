
create or replace function getpassword(par_username text) returns text as
$$
  declare
    loc_password text;
    loc_logged_in boolean;
  begin
     select into loc_password password from users where username = par_username;
     select into loc_logged_in logged_in from users where username = par_username;
     if loc_password isnull then
       loc_password = 'null';
     end if;
     if loc_logged_in isnotnull then
       update users set logged_in = 'True' where username = par_username;
     end if;
     return loc_password;
 end;
$$
 language 'plpgsql';

Create or replace function getUsers(in boolean, out int, out text, out text, out text) returns setof record as
$$
  select id, username, email, contact_number from users where deleted = $1;
$$
  language 'sql';
  
Create or replace function getServices(in boolean, out int, out int, out text, out text, out text) returns setof record as
$$
  select id, owner_id, name, type, info from services where deleted = $1;
$$
  language 'sql';

Create or replace function getPrices(in boolean, out int, out text, out int) returns setof record as
$$
  select id, svc_package, price from prices where deleted = $1;
$$
  language 'sql';

Create or replace function getEssentials(in boolean, out int, out text) returns setof record as
$$
  select id, essentials from essentials where deleted = $1;
$$
  language 'sql';

--User Functions--
CREATE or REPLACE FUNCTION addUser(par_username text, par_passhash text, par_email text, par_contact_number text) returns text as
$$
  declare
    loc_id text;
    loc_res text;
  begin
    select into loc_id id FROM users WHERE username = par_username;
    if loc_id isnull then
      insert into users (username, password, email, contact_number) values (par_username, par_passhash, par_email, par_contact_number);
      loc_res = 'OK';
    else
      loc_res = 'USER_EXISTS';
    end if;
    return loc_res;
  end;
$$
  language 'plpgsql';

create or replace function getUser(in text, out int, out text, out text, out text) returns setof record as
$$
  select id, username, email, contact_number
  from users where username = $1 and deleted = 'false';
$$
  language 'sql';
  
create or replace function storeToken(par_token text, par_username text) returns text as
$$
  declare
    loc_res text;
  begin
    select into loc_res username from users where username = par_username;
    if loc_res isnull then
      loc_res = 'null';
    else
      update users set app_token = par_token where username = par_username;
      loc_res = 'ok';
    end if;
    return loc_res;
  end;
$$
  language 'plpgsql';

create or replace function getUserById(in int, out text, out text, out text) returns setof record as 
$$
  select username, email, contact_number from users
    where id = $1 and deleted = 'false';;
$$
  language 'sql';

--Hires Functions--


--Favorites Functions--

--Events Functions--
create or replace function addEvent(par_name text, par_client int, par_mgr int, par_type text, par_due_date text) returns int as
$$
  declare
    loc_res int;
  begin
    insert into events (name, client_id, manager_id, event_type, due_date) VALUES (par_name, par_client, par_mgr, par_type, par_due_date);
    select into loc_res id from events where par_client = client_id;
    return loc_res;
 end;
$$
 language 'plpgsql';

create or replace function getEvent(in int, out text, out int, out int, out text, out text) returns setof record as 
$$
  select name, client_id, manager_id, event_type, due_date from events
    where id = $1 and deleted = 'false';;
$$
  language 'sql';

create or replace function getUserEvent(in int, out int, out text, out int, out int, out text, out text) returns setof record as 
$$
  select id, name, client_id, manager_id, event_type, due_date from events
    where client_id = $1 and deleted = 'false';;
$$
  language 'sql';


create or replace function getMgrEvent(in int, out text, out int, out int, out text, out text) returns setof record as 
$$
  select name, client_id, manager_id, event_type, due_date from events
    where manager_id = $1 and deleted = 'false';;
$$
  language 'sql';

--Event_Hires Functions--
create or replace function canvassService(par_event int, par_service int, par_package int) returns text as
$$
  declare
    loc_res text;
  begin
    insert into event_hires (event_id, svc_id) VALUES (par_event,par_service);
    insert into canvass (service_id, event_id, package_id) VALUES (par_service, par_event, par_package);
    loc_res = 'OK';
    return loc_res;
  end;
$$
 language 'plpgsql';

create or replace function hireService(par_event int, par_service int) returns text as
$$
  declare
    loc_res text;
  begin
    update event_hires set is_hired = 'true' where event_id = par_event and svc_id = par_service;
    loc_res = 'OK';
    return loc_res;
  end;
$$
 language 'plpgsql';

create or replace function getHires(in int, out int) returns int as 
$$
  select svc_id from event_hires
    where event_id = $1 and is_hired = 'true' and deleted = 'false';;
$$
  language 'sql';

create or replace function getCanvass(in int, out int, out int) returns setof record as 
$$
  select package_id, service_id from canvass
    where event_id = $1 and deleted = 'false';;
$$
  language 'sql';
    
--Services Functions--
create or replace function addService(par_name text, par_user int, par_type text) returns int as
$$
  declare
    loc_res int;
  begin
    insert into services (name, owner_id, type) VALUES (par_name, par_user, par_type);
    select into loc_res id from services where par_user = owner_id;
    return loc_res;
 end;
$$
 language 'plpgsql';

create or replace function addPackage(par_service int, par_price float, par_package text) returns text as
$$
  declare
    loc_res text;
  begin
    insert into prices (service_id, price, svc_package) VALUES (par_service, par_price, par_package);
    loc_res = 'OK';
    return loc_res;
 end;
$$
 language 'plpgsql';

create or replace function getServicePackages(in int, out int, out text, out int) returns setof record as 
$$
  select id, svc_package, price from prices
    where service_id = $1 and deleted = 'false';
$$
  language 'sql';

create or replace function getServicePackagesFromId(in int, out text, out int) returns setof record as 
$$
  select svc_package, price from prices
    where id = $1 and deleted = 'false';
$$
  language 'sql';

create or replace function sendMessage(par_sender int, par_receiver int, par_message text, par_timestamp text) returns text as
$$
  declare
    loc_res text;
  begin
    insert into messages (sender_id, receiver_id, msg_content, msg_timestamp) VALUES (par_sender, par_receiver, par_message, par_timestamp);
    loc_res = 'OK';
    return loc_res;
 end;
$$
 language 'plpgsql';

create or replace function getMessageFromUser(in int, in int, out int, out text, out text) returns setof record as 
$$
  select id, msg_content, msg_timestamp from messages
    where sender_id = $1 and receiver_id = $2 and deleted = 'false';;
$$
  language 'sql';

create or replace function getMessages(in int, out int, out int, out int, out text, out text) returns setof record as 
$$
  select id, receiver_id, sender_id, msg_content, msg_timestamp from messages
    where sender_id = $1 or receiver_id = $1 and deleted = 'false';;
$$
  language 'sql';

create or replace function reviewService(par_user int, par_service int, par_rating int, par_content text, par_timestamp text) returns text as
$$
  declare
    loc_res text;
  begin
    insert into reviews (user_id, service_id, rating, review_content, review_timestamp) VALUES (par_user, par_service, par_rating, par_content, par_timestamp);
    loc_res = 'OK';
    return loc_res;
 end;
$$
 language 'plpgsql';

create or replace function getServiceReviews(in int, out int, out int, out text, out int, out text) returns setof record as 
$$
  select id, rating, review_content, vote, review_timestamp from reviews
    where service_id = $1 and deleted = 'false';;
$$
  language 'sql';

create or replace function upvoteReview(par_review int) returns text as
$$
  declare
    loc_res text;
  begin
    update reviews set vote = vote+1 where par_review = id;
    loc_res = 'OK';
    return loc_res;
  end;
$$
 language 'plpgsql';

create or replace function downvoteReview(par_review int) returns text as
$$
  declare
    loc_res text;
  begin
    update reviews set vote = vote-1 where par_review = id;
    loc_res = 'OK';
    return loc_res;
  end;
$$
 language 'plpgsql';

create or replace function getService(in int, out int, out text, out text, out text) returns setof record as 
$$
  select owner_id, name, type, info from services
    where id = $1 and deleted = 'false';;
$$
  language 'sql';

create or replace function getEventHires(in int, out int) returns int as 
$$
  select svc_id from event_hires
    where is_hired = 'True' and event_id = $1 and deleted = 'false';;
$$
  language 'sql';

create or replace function getEventCanvass(in int, out int) returns int as 
$$
  select svc_id from event_hires
    where is_hired = 'False' and event_id = $1 and deleted = 'false';;
$$
  language 'sql';

create or replace function getUserService(in int, out int, out text, out text, out text) returns setof record as 
$$
  select id, name, type, info from services
    where owner_id = $1 and deleted = 'false';
$$
  language 'sql';
  
create or replace function delUser(p_user int) returns text as 
$$
  declare
    loc_res text;
  begin
    update users set deleted = 'true' where p_user = id;
    loc_res = 'OK';
    return loc_res;
  end;
$$
  language 'plpgsql';

create or replace function delEvent(p_event int) returns text as 
$$
  declare
    loc_res text;
  begin
    update events set deleted = 'true' where p_event = id;
    loc_res = 'OK';
    return loc_res;
  end;
$$
  language 'plpgsql';
  
create or replace function delService(p_service int) returns text as 
$$
  declare
    loc_res text;
  begin
    update services set deleted = 'true' where p_service = id;
    loc_res = 'OK';
    return loc_res;
  end;
$$
  language 'plpgsql';

create or replace function delPrice(p_price int) returns text as 
$$
  declare
    loc_res text;
  begin
    update prices set deleted = 'true' where p_price = id;
    loc_res = 'OK';
    return loc_res;
  end;
$$
  language 'plpgsql';

create or replace function delCanvass(p_canvass int) returns text as 
$$
  declare
    loc_res text;
  begin
    update canvass set deleted = 'true' where p_canvass = id;
    loc_res = 'OK';
    return loc_res;
  end;
$$
  language 'plpgsql';

create or replace function delReview(p_review int) returns text as 
$$
  declare
    loc_res text;
  begin
    update reviews set deleted = 'true' where p_review = id;
    loc_res = 'OK';
    return loc_res;
  end;
$$
  language 'plpgsql';

create or replace function delHire(p_hire int) returns text as 
$$
  declare
    loc_res text;
  begin
    update hires set deleted = 'true' where p_hire = id;
    loc_res = 'OK';
    return loc_res;
  end;
$$
  language 'plpgsql';

create or replace function delEventHire(p_event_hire int) returns text as 
$$
  declare
    loc_res text;
  begin
    update event_hires set deleted = 'true' where p_event_hire = id;
    loc_res = 'OK';
    return loc_res;
  end;
$$
  language 'plpgsql';

create or replace function delServicePrices(p_service_prices int) returns text as 
$$
  declare
    loc_res text;
  begin
    update service_prices set deleted = 'true' where p_service_prices = id;
    loc_res = 'OK';
    return loc_res;
  end;
$$
  language 'plpgsql';

create or replace function delEssentials(p_essential int) returns text as 
$$
  declare
    loc_res text;
  begin
    update essentials set deleted = 'true' where p_essential = id;
    loc_res = 'OK';
    return loc_res;
  end;
$$
  language 'plpgsql';
  
create or replace function editUser(p_id int, p_email text, p_contact_number text) returns text as 
$$
  declare
    loc_res text;
  begin
    update users set email = p_email, contact_number = p_contact_number where p_id = id;
    loc_res = 'OK';
    return loc_res;
  end;
$$
  language 'plpgsql';

create or replace function editService(p_id int, p_name text, p_type text, p_info text) returns text as 
$$
  declare
    loc_res text;
  begin
    update services set name = p_name, type = p_type, info = p_info where id = p_id;
    loc_res = 'OK';
    return loc_res;
  end;
$$
  language 'plpgsql';

create or replace function editEvent(p_id int, p_name text, p_manager_id int, p_event_type text, p_due_date text) returns text as 
$$
  declare
    loc_res text;
  begin
    update events set name = p_name, manager_id = p_manager_id, event_type = p_event_type, due_date = p_due_date where id = p_id;
    loc_res = 'OK';
    return loc_res;
  end;
$$
  language 'plpgsql';

create or replace function editPrices(p_id int, p_price int, p_svc_package text) returns text as 
$$
  declare
    loc_res text;
  begin
    update prices set price = p_price, svc_package = p_svc_package where id = p_id;
    loc_res = 'OK';
    return loc_res;
  end;
$$
  language 'plpgsql';

create or replace function editEssentials(p_id int, p_event_id int, p_essentials int) returns text as 
$$
  declare
    loc_res text;
  begin
    update essentials set essentials = p_essentials where p_id;
    loc_res = 'OK';
    return loc_res;
  end;
$$
  language 'plpgsql';

create or replace function editReview(p_id int, p_review_content text) returns text as 
$$
  declare
    loc_res text;
  begin
    update reviews set review_content = p_review_content where id = p_id;
    loc_res = 'OK';
    return loc_res;
  end;
$$
  language 'plpgsql';

create or replace function editHires(p_id int, p_start_date text, p_end_date text) returns text as 
$$
  declare
    loc_res text;
  begin
    update hires set start_date = p_start_date, end_date = p_end_date where id = p_id;
    loc_res = 'OK';
    return loc_res;
  end;
$$
  language 'plpgsql';
