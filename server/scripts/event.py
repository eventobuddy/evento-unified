from spcall import *
#import user
#import service

class Event(object):
  """ startdate should be a string that follows the format YYYY-MM-DD """
  def __init__(self, event, client):
    super(Event, self).__init__()
    self.client = client
    self.event_id = event
    self.name = None
    self.client = None
    self.manager = None
    self.hired_services = []
    self.canvassed_services = []
    self.date = None
    self.eventType = None
    self.details = None
    self.specifications = []

  def getEvent(self):
    return [{\
      'name' : self.name,\
      'id' : self.event_id,\
      'client' : self.client,\
      'manager' : self.manager,\
      'type' : self.eventType,\
      'date' : self.date,\
      'hired services' : self.hired_services,\
      'canvassed services' : self.canvassed_services,\
      'details' : self.details,\
      }]

  def getId(self):
    return self.event_id

  def setEventName(self, newName):
    self.name = newName

  def setEventManager(self, newManager):
    self.manager = newManager
    
  def setDueDate(self, due_date):
    self.dueDate = due_date

  def setEventType(self,event_type):
    self.eventType = event_type
        
  def store(self):
    if not self.name or not self.manager or not self.startDate:
      raise Exception('Not enough information.')
    else:
      params = (str(self.manager.getUserID()), str(self.name), str(self.startDate))
      return spcall("addEvent", params, True)

  def getServiceCategories(self):
    return self.service_category

  def addServiceCategory(self,name):
    self.service_category[str(name)] = []

  def removeServiceCategory(self,name):
    del self.serviceCategory[str(name)]

  def addService(self,category,id):
    service = Service()
    #service = DB function: search DB for service with id

    self.service_category[str(category)] + [service.id]

  def removeService(self,category,id):
    for service in self.service_category[str(category)]:
      if service.name == name:
        del service

  def viewService(self,id):
    for service in self.service_category[str(category)]:
      if service.id == id:
        return service

  def hireService(self,category,id):
    for service in self.service_category[str(category)]:
      if service.id == id:
        return service
