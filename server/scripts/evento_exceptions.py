"""User Registration and Authentication"""
class AlphaNumeric(Exception):
    def __init__(self,value):
        self.value = value
        self.code = 1

    def __str__(self):
        return repr(self.value)

class NameTaken(Exception):
    def __init__(self,value):
        self.value = value
        self.code = 2

    def __str__(self):
        return repr(self.value)

class WrongId(Exception):
    def __init__(self,value):
        self.value = value
        self.code = 3

    def __str__(self):
        return repr(self.value)

"""Event Management"""
class BlankField(Exception):
    def __init__(self,value):
        self.value = value
        self.code = 4

    def __str__(self):
        return repr(self.value)

class WrongAccount(Exception):
    def __init__(self,value):
        self.value = value
        self.code = 5

    def __str__(self):
        return repr(self.value)

class MissingEntry(Exception):
    def __init__(self,value):
        self.value = value
        self.code = 6

    def __str__(self):
        return repr(self.value)

class WrongCreator(Exception):
    def __init__(self,value):
        self.value = value
        self.code = 7

    def __str__(self):
        return repr(self.value)
