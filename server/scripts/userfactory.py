from spcall import spcall
from user import User

class UserFactory(object):
	@staticmethod
	def createUserFromUsername(username):
		params = (username,)
		user_data = spcall("getUser", params, True)
		# raise Exception(user_data)
		if len(user_data) == 0:
			raise LookupError('User not found')
		((
			user_id,
			username,
			email,
			contact_number
		),) = user_data
		user = User(user_id,username,email)
		user.setUserId(user_id)
		user.setUsername(username)
		user.setEmail(email)
		user.setContactNumber(contact_number)
		return user

	@staticmethod
	def createUser(username, password, email, contact_number):
	    params = (username, password, email, contact_number)
	    user_data = spcall("addUser", params, True)
	    
	    user = User()
	    user.setUsername(username)
	    user.setPassword(password)
	    user.setEmail(email)
	    user.setContactNumber(contact_number)
	    return user

	@staticmethod
	def createUserFromId(user_id):
		params = (user_id,)
		user_data = spcall("getUserById", params)
		if len(user_data) == 0:
			raise LookupError('User not found')
		((
		    user_id,
			username,
			email,
			contact_number
		),) = user_data
		user = User(user_id,username,email)
		user.setContactNumber(contact_number)
		return user

	@staticmethod
	def getPassword(username):
		params = (username, )
		user_data = spcall("getUser", params, False)
		return user_data[0][2]
