from spcall import *
from pbkdf2 import crypt

from datetime import datetime as date
from event_manager import EventManager

class User(object):
  def __init__(self, p_id,p_username, p_email):
	super(User, self).__init__()
	self.id = p_id
	self.username = p_username
	self.email = p_email
	self.contact_number = None
	self.eventManager = EventManager(self.getId())
	self.services = []
	self.messages = []
	self.appToken = None

  def upvote(self,review):
    from scripts.spcall import spcall
  
    return spcall("upvoteReview",(review,),True)

  def downvote(self,review):
    from scripts.spcall import spcall
  
    return spcall("downvoteReview",(review,),True)

  def review(self,p_service,p_rating,p_content):
    from scripts.spcall import spcall
    
    return spcall("reviewService", (self.id,p_service,p_rating,p_content,str(date.now())), True)

  def sendMessage(self,user2, content):
    from scripts.spcall import spcall
    from scripts.userfactory import UserFactory
    
    user = UserFactory.createUserFromUsername(user2)
    ((message,),) = spcall("sendMessage", (self.id, user.getId(), content, str(date.now())),True)
    return message
    
  def getMessagesFromUser(self,user2):
    from scripts.spcall import spcall
    from scripts.userfactory import UserFactory
    
    convo = []
    p_user2 = UserFactory.createUserFromUsername(user2)
    message = spcall("getMessageFromUser", (self.id, p_user2.id))
#    raise Exception(message)
    for i in range(0,len(message)):
      S = message[i]
      (msg_id,
        msg_content,
        msg_timestamp) = S
      convo = convo + [{
        'timestamp':msg_timestamp,
        'content':msg_content,
        'from':user2}]
#    raise Exception(convo)
    return convo

  def getMessages(self):
    from scripts.spcall import spcall
    
    message = spcall("getMessages", (self.id,))
#    raise Exception(message)
    for i in range(0,len(message)):
      S = message[i]
      (msg_id,
        msg_user0,
        msg_user1,
        msg_content,
        msg_timestamp) = S
      self.messages = self.messages + [{
        'content':msg_content,
        'timestamp':msg_timestamp,
        'sender':msg_user0,
        'receiver':msg_user1}]
    return self.messages

  def getUser(self):
    return {\
    'id':self.id,\
    'username':self.username,\
    'email':self.email,\
    'contact_number':self.contact_number,\
    'events':self.getEventManager().getUserEvent(),\
    'services':self.getServices(),\
    'messages':self.getMessages()}

  def editUser(self, email, contact_number):
    from spcall import spcall
    
    if email is None:
      email = self.email
    if contact_number is None:
      contact_number = self.contact_number
    result = spcall("editUser",(self.id,email,contact_number),True)
    return result

  def editService(self, svc_id, name, svc_type, info):
    from spcall import spcall
    from servicefactory import ServiceFactory

    service = ServiceFactory.createServiceFromId(svc_id)
    if name is None:
      name = service.name
    if svc_type is None:
      svc_type = service.type
    if info is None:
      info = service.info
    result = spcall("editService",(svc_id, name, svc_type, info),True)
    return result

  def editPrices(self, svc_id, price_id, price, package, hired_by):
    from spcall import spcall
    from servicefactory import ServiceFactory

    service = ServiceFactory.createServiceFromId(svc_id)
    if price is None:
      price = service.price
    if package is None:
      package = service.type
    if info is None:
      info = service.info
    result = spcall("editPrices",(price_id, price, svc_type, info),True)
    return result

  def getEventManager(self):
    return self.eventManager

  def addEvent(self,event,manager=1,event_type='None',date='None'):
    self.eventManager.addEvent(event,manager,event_type,date)
    
  def addService(self,service,service_type):
    from servicefactory import ServiceFactory
    
    service = ServiceFactory.createService(self.id,service,service_type)
    self.services = self.services + service.getService()
  
  def addServicePackage(self,service,package,price):
    from spcall import spcall
    from service import Service
    
    return {'status':spcall("addPackage",(service,price,package),True)}
  
  def getServicePackage(self,p_service):
    from spcall import spcall
    from servicefactory import ServiceFactory
    
    service = ServiceFactory.createServiceFromId(p_service)
    service_packages = spcall('getServicePackages',(p_service,),True)
    for j in range(0,len(service_packages)):
      P = service_packages[j]
      (p_package_id,
      p_package,
      p_price) = P
      service.packages = service.packages + [{
        'id':p_package_id,
        'package':p_package,
        'price':p_price}]
    return service.packages
  
  def editReview(self,review,content):
    from spcall import spcall
    
    status = spcall("editReview", (review,content), True)
    return status    
  
  def getServices(self):
    from spcall import spcall
    from service import Service
    
    reviews = []
    packages = []
    params = (self.id,)
    service_data = spcall('getUserService',params, True)
    for i in range(0,len(service_data)):
      L = service_data[i]
      (p_id,
      p_name,
      p_type,
      p_info) = L
      service = Service(self.id,p_name,p_type)
      service.id = p_id
      service.info = p_info

      service_review = spcall('getServiceReviews',(p_id,),True)
      #raise Exception(service_review)
      for j in range(0,len(service_review)):
        P = service_review[j]
        #raise Exception(P)
        (p_review_id,
        p_rating,
        p_content,
        p_vote,
        p_timestamp) = P
        reviews = reviews + [{
          'id':p_review_id,
          'rating':p_rating,
          'content':p_content,
          'vote':p_vote,
          'timestamp':p_timestamp}]
      #raise Exception(reviews)
      
      service_packages = spcall('getServicePackages',(p_id,),True)
      #raise Exception(service_review)
      for j in range(0,len(service_packages)):
        P = service_packages[j]
        #raise Exception(P)
        (p_package_id,
        p_package,
        p_price) = P
        packages = packages + [{
          'id':p_package_id,
          'package':p_package,
          'price':p_price}]
      #raise Exception(reviews)
      service.reviews = reviews
      service.packages = packages
      self.services = self.services + service.getService()
    return self.services

  def delService(self, service):
    from scripts.spcall import spcall
    
    status = spcall("delService", (service,), True)
    return status

  def delReview(self, review):
    from scripts.spcall import spcall
    
    status = spcall("delReview", (review,), True)
    return status

  def delPackage(self, package):
    from scripts.spcall import spcall
    
    status = spcall("delPrice", (package,), True)
    return status

  def getId(self):
    return self.id

  def getUsername(self):
    return self.username

  def getPassword(self):
    return self.password

  def getEmail(self):
    return self.email

  def getContactNumber(self):
    return self.contact_number

  def getRole(self):
    return self.role

  def getAppToken(self):
    return self.appToken

  def setUserId(self, user_id):
    self.id = user_id

  def setUsername(self, username):
    self.username = username

  def setPassword(self, password):
    self.password = password

  def setEmail(self, email):
    self.email = email

  def setContactNumber(self, contactNumber):
    self.contact_number = contactNumber
		
  def setRole(self, role):
    self.role = role

  def setAppToken(self, appToken):
    self.appToken = appToken

  def store(self):
    if not self.username or not self.password or not self.email or not self.role:
      raise Exception('Not enough information')
      params = (str(self.username), str(crypt(self.password)), str(self.email), str(self.role))
      ((self.user_id,),) = spcall("addUser", params, True)
      return True

  def storeToken(self):
    if not self.appToken:
      raise Exception('App token cannot be empty')
    params = (str(self.appToken), str(self.username))
    spcall("storeToken", params, True)
    return True

  def loginUser(self, username, password):
    from scripts.userfactory import UserFactory
    userFactory = UserFactory()
    passhash = userFactory.getPassword(username)
    if passhash == crypt(password, passhash):
      return 'ok'
    else:
      return 'KO'
