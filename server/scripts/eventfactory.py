from spcall import spcall
from userfactory import UserFactory
from event import Event

class EventFactory(object):
	@staticmethod
	def createEventFromId(event_id):
		params = (event_id)
		event_data = spcall("getEvent", params, True)
		if len(event_data) == 0:
			raise LookupError('Event not found')
		((
			name,
			client_id,
			manager_id,
			event_type,
			due_date
		),) = event_data
		event = Event(event_id,client_id)
		event.setEventName(name)
		event.setEventManager(manager_id)
		event.setDueDate(due_date)
		event.setEventType(event_type)
		return event
		
#	@staticmethod
#	def createEvent(eventName):
	    
