class Service(object):
  def __init__(self,p_owner,p_name,p_type):
    self.id = None
    self.owner = p_owner
    self.name = p_name
    self.type = p_type
    self.info = None
    self.reviews = []
    self.packages = []
    
  def getService(self):
    return [{\
      'id' : self.id,\
      'owner' : self.owner,\
      'name' : self.name,\
      'type' : self.type,\
      'info' : self.info,\
      'reviews': self.reviews,\
      'packages': self.packages}]
