from spcall import spcall
from userfactory import UserFactory
from service import Service

class ServiceFactory(object):
  @staticmethod
  def createService(p_owner,p_name,p_type):
    params = (p_name, p_owner, p_type)
    service_data = spcall("addService", params, True)
    
    service = Service(p_owner,p_name,p_type)
    service.id = ((service_data,),)
    return service
    
  @staticmethod
  def createServiceFromId(p_id):
    params = (p_id,)
    service_data = spcall("getService", params)
    if len(service_data) == 0:
      raise LookupError
    ((
      p_owner_id,
      p_name,
      p_type,
      p_info
    ),) = service_data
    service = Service(p_owner_id,p_name,p_type)
    service.id = p_id    
    service_packages = spcall('getServicePackages',(p_id,),True)
    for j in range(0,len(service_packages)):
      P = service_packages[j]
      (p_package_id,
      p_package,
      p_price) = P
      service.packages = service.packages + [{
        'id':p_package_id,
        'package':p_package,
        'price':p_price}]
    return service

  @staticmethod
  def createPriceFromId(p_id):
    params = (p_id,)
    price_data = spcall("getServicePackagesFromId", params)
    if len(price_data) == 0:
      raise LookupError
    ((
      p_package,
      p_price
    ),) = price_data
    return {'package':p_package, 'price':p_price, 'id':p_id}
    
  @staticmethod
  def createServiceFromUser(p_owner):
    params = (p_owner,)
    service_data = spcall("getUserService", params)
    if len(service_data) == 0:
      raise LookupError
    ((
      p_id,
      p_name,
      p_type,
      p_info
    ),) = service_data
    service = Service(p_owner,p_name,p_type)
    service.id = p_id
    return service

