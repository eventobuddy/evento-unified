class EventManager(object):
  def __init__(self,user):
    self.user = int(user)
    self.events = []

  def addEvent(self,name,manager,event_type,date):
    from event import Event
    from scripts.spcall import spcall

    p_user = self.user
    params = (str(name), p_user, manager, event_type, date)
    [(event,)] = spcall("addEvent", params, True)

    params = (event,)
    [(name,
      client_id,
      manager_id,
      event_type,
      due_date)] = spcall("getEvent", params, False)
    event_db = Event(event,client_id)
    event_db.setEventManager(manager_id)
    event_db.setEventType(event_type)
    event_db.setDueDate(due_date)
    self.events = self.events + [event_db.getEvent()]
    return event

  def getUserEvent(self):
    from event import Event
    from scripts.spcall import spcall

#    p_user = self.user
    hired_svcs = []
    canvassed_svcs = []
    params = (self.user,)
    eList = spcall("getUserEvent", params)
    for i in range(0,len(eList)):
      L = eList[i]
      (event_id,
        name,
        client_id,
        manager_id,
        event_type,
        due_date) = L

      (hired_services,) = spcall("getEventHires",(event_id,))
#      raise Exception(hired_services)
      for j in range(0,len(hired_services)):
        if hired_services[0] != None:
          S = spcall("getService",(hired_services[j],),True)
#          raise Exception(S[0])
          (p_owner_id,
            p_name,
            p_type,
            p_info) = S[0]
          hired_svcs = hired_svcs + [{
            'owner ID' : p_owner_id,
            'name': p_name,
            'type': p_type,
            'info': p_info}]
        else:
          hired_svcs = []

      canvassed_services = spcall("getCanvass",(event_id,))
#      raise Exception(canvassed_services[0][1])
      for j in range(0,len(canvassed_services)):
        service_packages = []
        if canvassed_services[j][1] != None:
#          raise Exception(canvassed_services[j][0])
          packages = spcall("getServicePackagesFromId",(canvassed_services[j][1],),True)
          for k in range(0,len(packages)):
            P = packages[k]
            (x_package,
            x_price) = P
            service_packages = service_packages + [{'package':x_package,'price':x_price}]
#          raise Exception(packages)
          Q = spcall("getService",(canvassed_services[j][0],),True)
#          raise Exception(Q)
          [(t_owner_id, t_name, t_type, t_info)] = Q
          canvassed_svcs = canvassed_svcs + [{
            'owner ID' : t_owner_id,
            'name': t_name,
            'type': t_type,
            'packages': service_packages,
            'info': t_info}]
        else:
          canvassed_svcs = []

      event = Event(event_id,client_id)
      event.name = name
      event.client = client_id
      event.manager = manager_id
      event.eventType = event_type
      event.date = due_date
      event.hired_services = hired_svcs
      event.canvassed_services = canvassed_svcs
      self.events = self.events + event.getEvent()
      hired_svcs = []
      canvassed_svcs = []
    return self.events
    
  def getMgrEvents(self):
    from event import Event
    from scripts.spcall import spcall

    p_user = self.user
    params = (p_user,)
    [(name,
      client_id,
      manager_id,
      event_type,
      due_date)] = spcall("getUserEvent", params, False)
    event_db = Event(event,client_id)
    event_db.setEventManager(manager_id)
    event_db.setEventType(event_type)
    event_db.setDueDate(due_date)
    self.events = self.events + [event_db.getEvent()]
    return event

  def canvassService(self,event,service,package):
    from spcall import spcall
  
    params = (service, event, package)
    ((canvass,),) = spcall('canvassService',params,True)
    return {'status':canvass}
    
  def getCanvass(self,event):
    from spcall import spcall
    from servicefactory import ServiceFactory
    from eventfactory import EventFactory
    from event import Event
    
    p_event = Event(event,self.user)
    params = (event,)
    packages = []
    hires = spcall('getCanvass',params)
#    raise Exception(hires)
    for i in range(0,len(hires)):
      (package_id,hires_id) = hires[i]
      service = ServiceFactory.createServiceFromId(hires_id)
      X = spcall("getServicePackagesFromId",(package_id,),True)
#      raise Exception(X)
      for k in range(0,len(X)):
        Z = X[k]
        (p_package,
        p_price) = Z
        service.packages = service.packages + [{'id':package_id,'package':p_package, 'price':p_price}]
      p_event.canvassed_services = p_event.canvassed_services + [service.getService()]
    return p_event.canvassed_services

  def hireService(self,event,service):
    from spcall import spcall
  
    params = (event, service)
    ((hire,),) = spcall('hireService',params,True)
    return {'status':hire}
    
  def getHires(self,event):
    from spcall import spcall
    from servicefactory import ServiceFactory
    from eventfactory import EventFactory
    from event import Event
    
    p_event = Event(event,self.user)
    params = (event,)
    hires = spcall('getHires',params)
    
    for i in range(0,len(hires)):
      (hires_id,) = hires[i]
      service = ServiceFactory.createServiceFromId(hires_id)
      p_event.hired_services = p_event.hired_services + [service.getService()]
    return p_event.hired_services

  def editEvent(self, event, name, manager_id, event_type, due_date):
    from spcall import spcall
    
    result = ("editEvent",(event,name,manager_id,event_type,due_date),True)
    return result

  def delEvent(self, event):
    from spcall import spcall
    
    status = spcall("delEvent",(event,),True)
    return status

  def delCanvass(self, canvass):
    from spcall import spcall
    
    status = spcall("delCanvass",(canvass,),True)
    return status
  
  def delHire(self, hire):
    from spcall import spcall
    
    status = spcall("delHire",(hire,),True)
    return status
