from spcall import *

def logIn(username,password):
  from scripts.userfactory import UserFactory

  params = (str(username), )
  [(user,)] = spcall("getpassword", params, True)
  if user != password:
    raise Exception('Wrong Input')
  user = UserFactory.createUserFromUsername(username)
  return {
    'status':'OK',
    'user':username,
    'id':user.getId()}

def register(username,password,email,contact_number):
  params = (username, password, email, contact_number)
  user_data = spcall("addUser", params, True)
  return {'status':user_data}
