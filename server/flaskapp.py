import os
from datetime import datetime
from scripts.model import DBconn
from flask import Flask, request, flash, url_for, redirect, \
     render_template, abort, send_from_directory, jsonify

app = Flask(__name__)
app.config.from_pyfile('flaskapp.cfg')

#User authentication
@app.route('/register', methods=['POST'])
def register():
  from scripts.userAuth import register
    
  data = request.get_json()
  username = data['username']
  password = data['password']
  email = data['email']
  contact_number = data['contact_number']
  try:
    reg = jsonify(register(username,password,email,contact_number))
  except:
    return jsonify({'status':'ERROR'})
  return reg

@app.route('/login', methods=['GET','POST'])
def login():
  from scripts.userAuth import logIn
  
  data = request.get_json()
  username = data['username']
  password = data['password']
  auth = None
  
  try:
    auth = logIn(username,password)
    #session['logged_in'] = True
  except:
    return jsonify({'status':'ERROR'})
#  return jsonify({'status':'OK'})
  return jsonify(auth)

@app.route('/register_html', methods=['POST'])
def registerHtml():
  from scripts.userAuth import register
    
  username = request.form['username']
  password = request.form['password']
  email = request.form['email']
  contact_number = request.form['contact_number']
  try:
    reg = jsonify(register(username,password,email,contact_number))
  except:
    return jsonify({'status':'ERROR'})
  return reg

@app.route('/login_html', methods=['GET','POST'])
def loginHtml():
  from scripts.userAuth import logIn
  
  username = request.form['username']
  password = request.form['password']
  
  try:
    auth = logIn(username,password)
#    session['logged_in'] = True
  except:
    return jsonify({'status':'ERROR'})
#  return jsonify({'status':'OK'})
  return redirect(url_for('home', p_username=username))

#Root screen
@app.route('/<string:p_username>')
def home(p_username):
  from scripts.user import User
  from scripts.userfactory import UserFactory
  
  user = UserFactory.createUserFromUsername(p_username)
#  eMan = user.getEventManager()
  return jsonify(user.getUser())

@app.route('/<string:p_username>/add/<string:p_event>')
def addEvent(p_username,p_event):
  from scripts.user import User
  from scripts.userfactory import UserFactory
  
  user = UserFactory.createUserFromUsername(p_username)
  user.addEvent(p_event)
  return jsonify({'event':user.getEventManager().getUserEvent()})

@app.route('/<string:p_username>/register/<string:p_service>/<string:p_type>')
def addService(p_username,p_service,p_type):
  from scripts.user import User
  from scripts.userfactory import UserFactory
  
  user = UserFactory.createUserFromUsername(p_username)
  user.addService(p_service,p_type)
  return jsonify({'service':user.getServices()})

#Services screen
@app.route('/<string:p_username>/services')
def viewService(p_username):
  from scripts.user import User
  from scripts.userfactory import UserFactory
  
  user = UserFactory.createUserFromUsername(p_username)
#  raise Exception(jsonify(user.getServices()))
  return jsonify({'services':user.getServices()})

@app.route('/<string:p_username>/services/<int:p_service>/addPackage/<string:p_package>/<int:p_price>')
def addServicePackage(p_username,p_service,p_package,p_price):
  from scripts.user import User
  from scripts.userfactory import UserFactory
  from scripts.spcall import spcall
  
  user = UserFactory.createUserFromUsername(p_username)
  return jsonify({'status':user.addServicePackage(p_service,p_package,p_price)})

@app.route('/<string:p_username>/services/<int:p_service>')
def getService(p_username,p_service):
  from scripts.user import User
  from scripts.userfactory import UserFactory
  from scripts.servicefactory import ServiceFactory
  from scripts.spcall import spcall
  
  user = UserFactory.createUserFromUsername(p_username)
  service = ServiceFactory.createServiceFromId(p_service)
  return jsonify({"service":service.getService()})

#Events Screen
@app.route('/<string:p_username>/events')
def getEvents(p_username):
  from scripts.user import User
  from scripts.userfactory import UserFactory
  
  user = UserFactory.createUserFromUsername(p_username)
  evtMan = user.getEventManager()
  return jsonify({'status':evtMan.getUserEvent()})

@app.route('/<string:p_username>/canvass/<int:p_event>/<int:p_service>/<int:p_package>')
def canvassService(p_username,p_service,p_event,p_package):
  from scripts.user import User
  from scripts.userfactory import UserFactory
  
  user = UserFactory.createUserFromUsername(p_username)
  evtMan = user.getEventManager()
  return jsonify(evtMan.canvassService(p_service,p_event,p_package))

#Canvassing
@app.route('/<string:p_username>/canvass/<int:p_event>')
def getCanvass(p_username,p_event):
  from scripts.user import User
  from scripts.userfactory import UserFactory
  
  user = UserFactory.createUserFromUsername(p_username)
  evtMan = user.getEventManager()
  return jsonify({'canvass':evtMan.getCanvass(p_event)})

@app.route('/<string:p_username>/hire/<int:p_event>/<int:p_service>')
def hireService(p_username,p_service,p_event):
  from scripts.user import User
  from scripts.userfactory import UserFactory
  
  user = UserFactory.createUserFromUsername(p_username)
  evtMan = user.getEventManager()
  return jsonify(evtMan.hireService(p_event,p_service))
  
@app.route('/<string:p_username>/hire/<int:p_event>')
def getHires(p_username,p_event):
  from scripts.user import User
  from scripts.userfactory import UserFactory
  
  user = UserFactory.createUserFromUsername(p_username)
  evtMan = user.getEventManager()
  return jsonify({'hires':evtMan.getHires(p_event)})
  
#Review Service
@app.route('/<string:p_username>/review/<int:p_service>/<int:p_rating>/<string:p_content>')
def reviewService(p_username,p_service,p_rating,p_content):
  from scripts.user import User
  from scripts.userfactory import UserFactory
  from scripts.spcall import spcall
  
  user = UserFactory.createUserFromUsername(p_username)
  service = user.review(p_service,p_rating,p_content)
  return jsonify({'status':service})

@app.route('/<string:p_username>/upvote/<int:p_review>')
def upvoteReview(p_username,p_review):
  from scripts.user import User
  from scripts.userfactory import UserFactory
  from scripts.spcall import spcall
  
  user = UserFactory.createUserFromUsername(p_username)
  return jsonify({'status':user.upvote(p_review)})

@app.route('/<string:p_username>/downvote/<int:p_review>')
def downvoteReview(p_username,p_review):
  from scripts.user import User
  from scripts.userfactory import UserFactory
  from scripts.spcall import spcall
  
  user = UserFactory.createUserFromUsername(p_username)
  return jsonify({'status':user.downvote(p_review)})

#Messaging
@app.route('/<string:p_username>/message/<string:p_receiver>/<string:p_content>')
def sendMessage(p_username,p_receiver,p_content):
  from scripts.user import User
  from scripts.userfactory import UserFactory
  from scripts.spcall import spcall
  
  user = UserFactory.createUserFromUsername(p_username)
  return jsonify({'status':user.sendMessage(p_receiver,p_content)})

@app.route('/<string:p_username>/message/<string:p_receiver>')
def getMessagesFromUser(p_username,p_receiver):
  from scripts.user import User
  from scripts.userfactory import UserFactory
  from scripts.spcall import spcall
  
  user = UserFactory.createUserFromUsername(p_username)
  return jsonify({'messages':user.getMessagesFromUser(p_receiver)})

@app.route('/<string:p_username>/message')
def getMessages(p_username):
  from scripts.user import User
  from scripts.userfactory import UserFactory
  from scripts.spcall import spcall
  
  user = UserFactory.createUserFromUsername(p_username)
  return jsonify({'messages':user.getMessages()})

@app.route('/<string:p_username>/edit/mail/<string:p_email>')
def editUserMail(p_username, p_email):
  from scripts.userfactory import UserFactory
  
  user = UserFactory.createUserFromUsername(p_username)
  return jsonify({'status':user.editUser(p_email,user.contact_number)})

@app.route('/<string:p_username>/edit/number/<string:p_contact_number>')
def editUserNumber(p_username, p_contact_number):
  from scripts.userfactory import UserFactory
  
  user = UserFactory.createUserFromUsername(p_username)
  return jsonify({'status':user.editUser(user.email,p_contact_number)})

@app.route('/<string:p_username>/services/<int:p_service>/edit/name/<string:p_name>')
def editServiceName(p_username, p_service, p_name):
  from scripts.userfactory import UserFactory
  from scripts.servicefactory import ServiceFactory
  
  user = UserFactory.createUserFromUsername(p_username)
  service = ServiceFactory.createServiceFromId(p_service)
  return jsonify({'status':user.editService(p_service,p_name,service.type,service.info)})

@app.route('/<string:p_username>/services/<int:p_service>/edit/type/<string:p_type>')
def editServiceType(p_username, p_service, p_type):
  from scripts.userfactory import UserFactory
  from scripts.servicefactory import ServiceFactory
  
  user = UserFactory.createUserFromUsername(p_username)
  service = ServiceFactory.createServiceFromId(p_service)
  return jsonify({'status':user.editService(p_service,service.name,p_type,service.info)})

@app.route('/<string:p_username>/services/<int:p_service>/edit/info/<string:p_info>')
def editServiceInfo(p_username, p_service, p_info):
  from scripts.userfactory import UserFactory
  from scripts.servicefactory import ServiceFactory
  
  user = UserFactory.createUserFromUsername(p_username)
  service = ServiceFactory.createServiceFromId(p_service)
  return jsonify({'status':user.editService(p_service,service.name,service.type,p_info)})

@app.route('/<string:p_username>/services/<int:p_service>/packages')
def getPackages(p_username,p_service):
  from scripts.userfactory import UserFactory
  from scripts.servicefactory import ServiceFactory
  
  user = UserFactory.createUserFromUsername(p_username)
  return jsonify({'packages':user.getServicePackage(p_service)})

@app.route('/<string:p_username>/services/<int:p_service>/packages/<int:p_package>')
def getPackage(p_username,p_service,p_package):
  from scripts.userfactory import UserFactory
  from scripts.servicefactory import ServiceFactory
  
  user = UserFactory.createUserFromUsername(p_username)
  return jsonify({'packages':ServiceFactory.createPriceFromId(p_package)})

@app.route('/<string:p_username>/services/<int:p_service>/packages/<int:p_price_id>/edit/package/<string:p_package>')
def editPackage(p_username, p_service, p_price_id, p_package):
  from scripts.userfactory import UserFactory
  
  user = UserFactory.createUserFromUsername(p_username)
  price = ServiceFactory.createPriceFromId(p_price_id)
  return jsonify({'status':user.editPrices(p_service,p_price_id,price.price,p_package)})

@app.route('/<string:p_username>/services/<int:p_service>/packages/<int:p_price_id>/edit/price/<int:p_price>')
def editPrice(p_username, p_service, p_price_id, p_price):
  from scripts.userfactory import UserFactory
  
  user = UserFactory.createUserFromUsername(p_username)
  price = ServiceFactory.createPriceFromId(p_price_id)
  return jsonify({'status':user.editPrices(p_service,p_price_id,p_price,price.package)})

@app.route('/<string:p_username>/review/<int:p_review>/edit/<string:p_content>')
def editReview(p_username, p_review, p_content):
  from scripts.userfactory import UserFactory
  
  user = UserFactory.createUserFromUsername(p_username)
  return jsonify({'status':user.editReview(p_review,p_content)})

@app.route('/<string:p_username>/event/<int:p_event>/edit/name/<string:p_name>')
def editEventName(p_username, p_event, p_name):
  from scripts.userfactory import UserFactory
  from scripts.eventfactory import EventFactory
  
  user = UserFactory.createUserFromUsername(p_username)
  evtMan = user.getEventManager()
  event = EventFactory.createEventFromId(p_event)
  return jsonify({'status':evtMan.editEvent(p_event,p_name,event.manager_id,event.event_type,event.due_date)})

@app.route('/<string:p_username>/event/<int:p_event>/edit/manager/<int:p_manager>')
def editEventManager(p_username, p_event, p_manager):
  from scripts.userfactory import UserFactory
  from scripts.eventfactory import EventFactory
  
  user = UserFactory.createUserFromUsername(p_username)
  evtMan = user.getEventManager()
  event = EventFactory.createEventFromId(p_event)
  return jsonify({'status':evtMan.editEvent(p_event,event.name,p_manager,event.event_type,event.due_date)})
  
@app.route('/<string:p_username>/event/<int:p_event>/edit/type/<string:p_type>')
def editEventType(p_username, p_event, p_type):
  from scripts.userfactory import UserFactory
  from scripts.eventfactory import EventFactory
  
  user = UserFactory.createUserFromUsername(p_username)
  evtMan = user.getEventManager()
  event = EventFactory.createEventFromId(p_event)
  return jsonify({'status':evtMan.editEvent(p_event,event.name,event.manager_id,p_type,event.due_date)})
  
@app.route('/<string:p_username>/event/<int:p_event>/edit/due/<string:p_due>')
def editEventDue(p_username, p_event, p_due):
  from scripts.userfactory import UserFactory
  from scripts.eventfactory import EventFactory
  
  user = UserFactory.createUserFromUsername(p_username)
  evtMan = user.getEventManager()
  event = EventFactory.createEventFromId(p_event)
  return jsonify({'status':evtMan.editEvent(p_event,event.name,event.manager_id,event.event_type,p_due)})

#@app.route('/<string:p_username>/event/hires/edit/<int:p_hire>/<string:p_start>')
#def editEventHires(p_username, p_hire, p_due):
#  from scripts.userfactory import UserFactory
#  from scripts.eventfactory import EventFactory
#  
#  user = UserFactory.createUserFromUsername(p_username)
#   evtMan = user.getEventManager()
#  event = EventFactory.createEventFromId(p_event)
#  return jsonify({'status':evtMan.editEvent(p_event,event.name,event.manager_id,event.event_type,p_due)})

@app.route('/<string:p_username>/events/delete/<int:p_event>')
def delEvent(p_username, p_event):
  from scripts.user import User
  from scripts.userfactory import UserFactory
  from scripts.spcall import spcall
  
  user = UserFactory.createUserFromUsername(p_username)
  evtMan = user.getEventManager()
  return jsonify(evtMan.delEvent(p_event))

@app.route('/<string:p_username>/services/delete/<int:p_service>')
def delService(p_username, p_service):
  from scripts.user import User
  from scripts.userfactory import UserFactory
  from scripts.spcall import spcall
  
  user = UserFactory.createUserFromUsername(p_username)
  return jsonify(user.delService(p_service))

@app.route('/<string:p_username>/user/delete/<int:p_user>')
def delUser(p_username, p_user):
  return

@app.route('/<string:p_username>/review/delete/<int:p_review>')
def delReview(p_username, p_review):
  from scripts.user import User
  from scripts.userfactory import UserFactory
  from scripts.spcall import spcall
  
  user = UserFactory.createUserFromUsername(p_username)
  return jsonify(user.delReview(p_review))

@app.route('/<string:p_username>/package/delete/<int:p_package>')
def delPackage(p_username, p_package):
  from scripts.user import User
  from scripts.userfactory import UserFactory
  from scripts.spcall import spcall
  
  user = UserFactory.createUserFromUsername(p_username)
  return jsonify(user.delPackage(p_package))

@app.route('/<string:p_username>/canvass/<int:p_event>/delete/<int:p_canvass>')
def delCanvass(p_username, p_event, p_canvass):
  from scripts.user import User
  from scripts.userfactory import UserFactory
  from scripts.spcall import spcall
  
  user = UserFactory.createUserFromUsername(p_username)
  evtMan = user.getEventManager()
  return jsonify(evtMan.delCanvass(p_canvass))

@app.route('/<string:p_username>/hire/<int:p_event>/delete/<int:p_hire>')
def delHire(p_username, p_event, p_hire):
  from scripts.user import User
  from scripts.userfactory import UserFactory
  from scripts.spcall import spcall
  
  user = UserFactory.createUserFromUsername(p_username)
  evtMan = user.getEventManager()
  return jsonify(evtMan.delHire(p_hire))

@app.route('/search')
def search():
  from scripts.spcall import spcall

  usr = []  
  svc = []
  esn = []
  prc = []
  users = spcall("getUsers",(False,),True)
#  raise Exception(users)
  for i in range(0,len(users)):
    (user_id,
    user_name,
    user_email,
    user_number) = users[i]
#    raise Exception(users[i])
    usr = usr + [{'id':user_id,
    'username':user_name,
    'email':user_email,
    'contact_number':user_number}]

  services = spcall("getServices",(False,),True)
  for i in range(0,len(services)):
    (service_id,
    service_owner_id,
    service_name,
    service_type,
    service_info) = services[i]
    svc = svc + [{'id':service_id,
    'owner_id':service_owner_id,
    'name':service_name,
    'type':service_type,
    'info':service_info}]

  prices = spcall("getPrices",(False,),True)
  for i in range(0,len(prices)):
    (price_id,
    price_name,
    price_amount) = prices[i]
    prc = prc + [{'id':price_id,
    'name':price_name,
    'amount':price_amount}]
    
  essentials = spcall("getEssentials",(False,),True)
  for i in range(0,len(essentials)):
    (essential_id,
    essential_content) = essentials[i]
    esn = esn + [{'id':essential_id,
    'content':essential_content}]

  return jsonify({'users':usr,'services':svc,'prices':prc,'essentials':esn})

#Misc
@app.route('/')
def index():
  #return jsonify({'status':'ok'})
  return render_template('index.html')

@app.route('/<path:resource>')
def serveStaticResource(resource):
  return send_from_directory('static/', resource)

@app.route("/test")
def test():
  return "<strong>It's Alive!</strong>"

@app.after_request
def add_cors(resp):
    resp.headers['Access-Control-Allow-Origin'] = request.headers.get('Origin', '*')
    resp.headers['Access-Control-Allow-Credentials'] = True
    resp.headers['Access-Control-Allow-Methods'] = 'POST, OPTIONS, GET, PUT, DELETE'
    resp.headers['Access-Control-Allow-Headers'] = request.headers.get('Access-Control-Request-Headers',
                                                                             'Authorization')
    #set low for debugging

    if app.debug:
        resp.headers["Access-Control-Max-Age"] = '1'
    return resp

if __name__ == '__main__':
    app.run(debug=True)
