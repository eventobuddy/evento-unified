from setuptools import setup

setup(name='FlaskApp',
      version='1.0',
      description='A basic Flask app with static files',
      author='Ryan Jarvinen',
      author_email='ryanj@redhat.com',
      url='http://www.python.org/sigs/distutils-sig/',
      install_requires=['Flask>=0.10.1','SQLAlchemy>=1.0.8','Flask-SQLAlchemy==2.0','psycopg2==2.5.4','pbkdf2>=1.3','paver>=1.2.4', 'lettuce>=0.2.20','Flask-HTTPAuth==2.7.0'],
     )
