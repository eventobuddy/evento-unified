@feature.manageEvents
Feature: Manage Events. Event Managers can create, manage or delete existing events which is to be managed by this module.
	       As an Event Manager, I want to be able to create, manage and or Events

@manageEvents.addEvent
Scenario: Add Event
          Given "manager" is role "2"
          When "manager" enters addEvent function with value "Event"
          Then event with name "Event" is created

@manageEvents.addEventWrongInput
Scenario: Add Event - Wrong Input
		  Given "manager" is role "2"
          When "manager" enters addEvent function with value
		  |value|
		  |!&#%*|
		  |     |
          Then exception code "1" is thrown

@manageEvents.addEventExisting
Scenario: Add Event - Existing
		  Given "manager" is role "2"
          And "Event" already exists
          When "manager" enters addEvent function with value "Event"
		  Then exception code "2" is thrown

@manageEvents.addEventWrongPrivilege
Scenario: Add Event - Wrong Privilege
		  Given user roles are as follows
		  |username|role|
		  |client  |1   |
		  |service |3   |
		  When user enters addEvent function with value "Event"
		  Then exception code "5" is thrown

@manageEvents.removeEvent
Scenario: Remove Event
		  Given "manager" is role "2"
          And "Event" already exists
          When "manager" enters delEvent function with id value of "Event"
          Then "successful removal" notification is thrown.

@manageEvents.removeEventEmptyEvent
Scenario: Remove Event - Nonexisting Event
		  Given "manager" is role "2"
          And "Event" does not exist
          When "manager" enters delEvent function with id value of "Event"
		  Then exception code "6" is thrown

@manageEvents.removeEventWrongManager
Scenario: Remove Event - Wrong Manager
		  Given "manager" is role "2"
          And "Event" already exists
          And "Event" was made by "user2"
          When "manager" enters delEvent function with id value of "Event"
		  Then exception code "7" is thrown

@manageEvents.removeEventWrongPrivelege
Scenario: Remove Event - Wrong Privelege
		  Given user roles are as follows
		  |username|role|
		  |client  |1   |
		  |service |3   |
          And "Event" already exists
          When user enters delEvent function with id value of "Event"
	      Then exception code "5" is thrown
