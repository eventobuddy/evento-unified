@feature.userAuth
Feature: User Authentication. User can create, and log into their accounts to access their respective features in the app.

@userAuth.register
Scenario: Register
          When user registers with the credentials
          |username|password|email          |role|
          |user    |pswd123 |u.s-er@mail.com|1   |
	      Then the account has been created

@userAuth.registerWrongChara
Scenario: Register - Wrong Characters
          When user registers with the credentials
          |username|password|email          |role|
          |*&#^    |pswd123 |u.s-er@mail.com|1   |
          |user    |pswd123 |@*$^@*&%$*@&%$*|1   |
          |user    |*@&^$$* |u.s-er@mail.com|1   |
          Then exception code "1" is thrown

@userAuth.registerExistingName
Scenario: Register - Existing Username
          Given user exists
          |username|password|email          |role|
          |user    |pswd123 |u.s-er@mail.com|1   |
          When user registers with the credentials
          |username|password|email          |role|
          |user    |pswd123 |nouser@mail.com|1   |
          Then exception code "2" is thrown

@userAuth.logIn
Scenario: Log-In
          Given user exists.
          |username|password|
          |user    |pswd123 |
          When user logs in with the credentials
          |username|password|
          |user    |pswd123 |
          Then the user is logged into his account.

@userAuth.logInWrongInput
Scenario: Log-In - Wrong Input
          When user logs in with the credentials
          |username|password|
          |        |pswd123 |
          |user    |        |
          |! #%    |pswd123 |
          |user    |&@  ^*$ |
          Then exception code "1" is thrown

@userAuth.logInWrongUser
Scenario: Log-In - Wrong Entry
          Given user exists
          |username|password|
          |user    |pswd123 |
          When user logs in with the credentials
          |username|password|
          |aser    |pswd123 |
          |user    |notpswd |
          Then exception code "3" is thrown
