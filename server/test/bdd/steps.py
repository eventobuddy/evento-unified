from lettuce import step, world
from webtest import *
from flask import *
from nose.tools import assert_equal, assert_raises, raises
import json

world_vars = {
    'db_user':None,
    'db_credentials':None,
    'notif':None
}

#
# USER AUTHENTICATION TESTS
#

@step(u'Given user exists')
def given_user_already_exists(step):
    from scripts.userfactory import UserFactory
    from flaskapp import app
#    from flask import json

    for row in step.hashes:
        world.username = row['username']
        world.password = row['password']

    world.browser = TestApp(app)
    world.response = world.browser.get('http://localhost:5000/api/v1/view/'+\
        str(world.username))
    print world.response
    assert_equal(world.response.status_code,200)

@step(u'When user registers with the credentials')
def when_user_registers_with_the_credentials(step):
    from flaskapp import app
    from scripts.userfactory import UserFactory
    from flask import json

    for row in step.hashes:
        world.username = row['username']
        world.password = row['password']
        world.email = row['email']
        world.role = row['role']

    world.browser = TestApp(app)
    world.response = world.browser.get('http://localhost:5000/api/v1/register/'+\
        str(world.username)+'/'+\
        str(world.password)+'/'+\
        str(world.email)+'/'+\
        str(world.role))
    print world.response
    assert UserFactory.createUserFromId(world.response['user_id'])

@step(u'Then the account has been created')
def then_the_account_has_been_created(step):
    from flaskapp import app
    from scripts.userfactory import UserFactory

    world.response = world.browser.get('http://localhost:5000/api/v1/view/'+str(world.username))

    assert_equal(world.response.status_code,200)

@step(u'Then exception code "(.*)" is thrown')
@raises(Exception)
def then_exception_code_int1_is_thrown(step,code):
    assert_equal(Exception.code,code)

@step(u'Then "(.*)" notification is thrown')
def then_exception_code_int1_is_thrown(step,notif):
    assert_equal('ok',world_vars['notif']['status'])

@step(u'When user logs in with the credentials')
def when_user_logs_in_with_the_credentials(step):
    from flaskapp import app

    for row in step.hashes:
        world.username = row['username']
        world.password = row['password']

    world.browser = TestApp(app)
    world.response = world.browser.get('http://localhost:5000/api/v1/login/'+\
        str(world.username)+'/'+\
        str(world.password))
    assert_equal(world.response.status_code, 200)

@step(u'Then the user is logged into his account')
def then_the_user_is_logged_into_his_account(step):
    from flaskapp import app

    world.browser = TestApp(app)
    world.response = world.browser.get('http://localhost:5000/api/v1/'+str(world.username))
    assert_equal(world.response.status_code, 200)

#
# MANAGE EVENT TESTS
#

@step(u'Given "(.*)" is role "(.*)"')
def given_user1_is_role_role1(step,user,role):
    from flaskapp import app

    world.browser = TestApp(app)
    world.response = world.browser.get('http://localhost:5000/api/v1/'+str(user))
    assert_equal(world.response.status_code, 200)

@step(u'Given user roles are as follows')
def given_user1_is_role_role1(step):
    from scripts.userfactory import UserFactory
    from flaskapp import app

    for row in step.hashes:
        world.username = row['username']
        world.role = row['role']

    world.browser = TestApp(app)
    world.response = world.browser.get('http://localhost:5000/api/v1/'+\
        str(world.username)+'/'+\
        str(world.role))
    assert_equal(world.response.status_code, 200)

    world.user = UserFactory.createUserFromUsername(world.username)
#    assert_equal(world.user.getRole(),world.role)

@step(u'When "(.*)" enters addEvent function with value')
def when_user1_enters_addEvent_with_value(step,user):
    from scripts.userfactory import UserFactory

    for row in step.hashes:
        world.value = row['value']

    user = UserFactory.createUserFromUsername(user)
    world.browser = TestApp(app)
    world.response = world.browser.get('http://localhost:5000/api/v1/'+str(user))
    assert_equal(world.response.status_code, 200)

@step(u'When user enters addEvent function with value "(.*)"')
def when_user1_enters_addEvent_with_value_param1(step,param):
    from scripts.userfactory import UserFactory

    assert world.user.addEvent(param)

@step(u'When "(.*)" enters delEvent function with id value of "(.*)"')
def when_user1_enters_addEvent_with_value_param1(step,param):
    from scripts.eventfactory import EventFactory

    world.event = EventFactory.createEventFromName(param)
    assert world.user.delEvent(world.event.getId())

@step(u'When "(.*)" enters addEvent function with value "(.*)"')
def when_user1_enters_addEvent_with_value_param1(step,user,param):
    from scripts.userfactory import UserFactory

    user = UserFactory.createUserFromUsername(user)
    world.browser = TestApp(app)
    world.response = world.browser.get('http://localhost:5000/api/v1/'+\
        str(user)+'/'+\
        'addEvent/'+\
        str(param))
    assert_equal(world.response.status_code, 200)

@step(u'When "(.*)" enters delEvent function with id value of "(.*)"')
def when_user1_enters_delEvent_with_id_value_of_param1(step,user,param):
    from scripts.userfactory import UserFactory
    from scripts.eventfactory import EventFactory

    event = EventFactory.createEventFromUsername(param)
    event_id = event.getId()
    user = UserFactory.createUserFromUsername(user)
    world_vars['notif'] = user.delEvent(event_id)
    assert world_vars['notif']

@step(u'When "(.*)" enters delEvent function with id value of "(.*)"')
def when_user1_enters_delEvent_with_id_value_of_param1(step,user,param):
    from scripts.userfactory import UserFactory
    from scripts.eventfactory import EventFactory

    event = EventFactory.createEventFromUsername(param)
    event_id = event.getId()
    user = UserFactory.createUserFromUsername(user)
    world_vars['notif'] = user.delEvent(event_id)
    assert world_vars['notif']

@step(u'Then event with name "(.*)" is created')
def then_user_with_name_event1_is_created(step,event):
    from scripts.eventfactory import EventFactory

    assert EventFactory.createEventFromName(event)

@step(u'And "(.*)" was made by "(.*)"')
def and_event1_already_was_made_by_user1(step,event,user):
    from scripts.userfactory import UserFactory
    from scripts.eventfactory import EventFactory

    event = EventFactory.createEventFromName(event)
    user = UserFactory.createUserFromUsername(user)
    asert_equals(event.getCreatorId(),user.getId())

@step(u'And "(.*)" already exists')
def and_event1_already_exists(step,event):
    from scripts.eventfactory import EventFactory

    world.browser = TestApp(app)
    world.response = world.browser.get('http://localhost:5000/api/v1/'+\
        str(event))
    assert_equal(world.response.status_code, 200)
    assert EventFactory.createEventFromName(event)

@step(u'And "(.*)" does not exist')
def and_event1_does_exists(step,event):
    from scripts.eventfactory import EventFactory
    from scripts.eventoexceptions import MissingEntry
    assert_raises(MissingEntry,EventFactory.createEventFromName,event)

@step(u'When "(.*)" enters addServiceCategory function with value "(.*)"')
def when_user1_addServiceCategory(step,user,value):
    from flaskapp import addServiceCategory, loginUser
    from scripts.userfactory import UserFactory
    # from scripts.evento_exceptions import

    test_user = UserFactory.createUserFromUsername(user)
    loginUser(user,)
    service_cat = addServiceCategory(value)
    assert_equals(service_cat.getName(),value)

@step(u'When "(.*)" enters editServiceCategory function with value "(.*)"')
def when_user1_addServiceCategory(step,user,value):
    from flaskapp import addServiceCategory, loginUser
    from scripts.userfactory import UserFactory
    # from scripts.evento_exceptions import

    test_user = UserFactory.createUserFromUsername(user)
    loginUser(user,)
    service_cat = addServiceCategory("Service Category")
    service_cat.editName(value)
    assert_equals(service_cat.getName(),value)

@step(u'When "(.*)" enters addService function with value "(.*)"')
def when_user1_addServiceCategory(step,user,value):
    from flaskapp import addServiceCategory, loginUser
    from scripts.userfactory import UserFactory
    # from scripts.evento_exceptions import

    test_user = UserFactory.createUserFromUsername(user)
    loginUser(user,)
    service_cat = addServiceCategory(value)
    assert_equals(service_cat.getName(),value)
