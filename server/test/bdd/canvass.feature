@feature.eventCanvassing
Feature: Event Canvassing. Event Managers can add or remove service categories, search for and add a service for an event.

@eventCanvassing.AddServiceCategory
Scenario: Add Service Category
          Given "manager" is role "2"
          When "manager" enters addServiceCategory function with value "Service Category"
          Then service category will be added with value "Service Category"

@eventCanvassing.EditServiceCategory
Scenario: Edit Service Category
          Given "manager" is role "2"
          And Service Category with id "1" exists
          When "manager" enters editServiceCategory function with id value "1" and label "new category"
          Then Service Category id "1" will be modified with label "new category"

@eventCanvassing.RemoveServiceCategory
Scenario: Remove Service Category
          Given "manager" is role "2"
          And Service Category with id "1" exists
          When "manager" enters delServiceCategory function with id value "1"
          Then Service Category will be removed with id value "1"

@eventCanvassing.AddService
Scenario: Add Service
          Given "service category" exists
          When "manager" enters addService
          Then "manager" is redirected to "search"

@eventCanvassing.AddService
Scenario: Replace Service
          Given "service" exists
          When "manager" enters replaceService
          Then "manager" is redirected to "search"

@eventCanvassing.RemoveService
Scenario: Remove Service
          Given "service" exists
          When "manager" enters deleteService
          Then "service" is removed

@eventCanvassing.HireService
Scenario: Hire Service
          Given "service" exists
          When "manager" enters hireService
          Then "service" is added to "Service Category" "hired" list

@eventCanvassing.ViewServiceInformation
Scenario: View Service Information
          Given "service" exists
          When "user" enters viewServiceInformation for "service"
          Then the function returns service information
