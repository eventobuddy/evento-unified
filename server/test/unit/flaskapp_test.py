import unittest
from flask.ext.testing import TestCase
from flaskapp import app

class TestApp(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()

    def test_instance(self):
        app.config['TESTING'] = True
        return app

    def test_register(self):
        test = self.app.post('/register_html',data=dict(username='user',password='pswd123',email='user.mail@mail.com',contact_number='09654322452'), follow_redirects=True)
        assert AttributeError

    def test_view(self):
        test = self.app.post('/user',data=dict(username='user'), follow_redirects=True)
        assert AssertionError

    def test_login(self):
        test = self.app.post('/api/login',data=dict(username='user'), follow_redirects=True)
        assert AssertionError
    
    def test_add_event(self):
        test = self.app.post('/user/add/event',data=dict(username='user'), follow_redirects=True)
        assert AssertionError
    
    def test_add_service(self):
        test = self.app.post('/user/register/service/type',data=dict(username='user'), follow_redirects=True)
        assert AssertionError

    def test_view_service(self):
        test = self.app.post('/user/services',data=dict(username='user'), follow_redirects=True)
        assert AssertionError

    def test_add_service_package(self):
        test = self.app.post('/user/services/1/addPackage/packagelabel/12',data=dict(username='user'), follow_redirects=True)
        assert AssertionError

    def test_get_service(self):
        test = self.app.post('/user/services/1',data=dict(username='user'), follow_redirects=True)
        assert AssertionError

    def test_get_event(self):
        test = self.app.post('/user/events',data=dict(username='user'), follow_redirects=True)
        assert AssertionError

    def test_canvass_service(self):
        test = self.app.post('/user/canvass/1/1/1',data=dict(username='user'), follow_redirects=True)
        assert AssertionError
        
    def test_get_canvass(self):
        test = self.app.post('/user/canvass/1',data=dict(username='user'), follow_redirects=True)
        assert AssertionError
        
    def test_hire_service(self):
        test = self.app.post('/user/hire/1/1',data=dict(username='user'), follow_redirects=True)
        assert AssertionError
        
    def test_get_hires(self):
        test = self.app.post('/user/hire/1',data=dict(username='user'), follow_redirects=True)
        assert AssertionError
        
    def test_review_service(self):
        test = self.app.post('/user/review/1/1/reviewcontent',data=dict(username='user'), follow_redirects=True)
        assert AssertionError

    def test_upvote_review(self):
        test = self.app.post('/user/upvote/1',data=dict(username='user'), follow_redirects=True)
        assert AssertionError
        
    def test_downvote_review(self):
        test = self.app.post('/user/downvote/1',data=dict(username='user'), follow_redirects=True)
        assert AssertionError

    def test_send_message(self):
        test = self.app.post('/user/message/heuser/messagecontent',data=dict(username='user'), follow_redirects=True)
        assert AssertionError
        
    def test_get_user_messages(self):
        test = self.app.post('/user/message/heuser',data=dict(username='user'), follow_redirects=True)
        assert AssertionError
        
    def test_get_messages(self):
        test = self.app.post('/user/message',data=dict(username='user'), follow_redirects=True)
        assert AssertionError

    def test_edit_email(self):
        test = self.app.post('/user/edit/mail/newmail',data=dict(username='user'), follow_redirects=True)
        assert AssertionError

    def test_edit_number(self):
        test = self.app.post('/user/edit/number/0841235123',data=dict(username='user'), follow_redirects=True)
        assert AssertionError
        
    def test_edit_service_type(self):
        test = self.app.post('/user/services/1/edit/type/newtype',data=dict(username='user'), follow_redirects=True)
        assert AssertionError
        
    def test_edit_service_info(self):
        test = self.app.post('/user/services/1/edit/info/newinfo',data=dict(username='user'), follow_redirects=True)
        assert AssertionError
        
    def test_get_service_packages(self):
        test = self.app.post('/user/services/1/packages',data=dict(username='user'), follow_redirects=True)
        assert AssertionError
        
    def test_get_service_package(self):
        test = self.app.post('/user/services/1/packages/1',data=dict(username='user'), follow_redirects=True)
        assert AssertionError
        
    def test_edit_package(self):
        test = self.app.post('/user/services/1/packages/1/edit/package/newpackage',data=dict(username='user'), follow_redirects=True)
        assert AssertionError
        
    def test_edit_price(self):
        test = self.app.post('/user/services/1/packages/1/edit/price/50',data=dict(username='user'), follow_redirects=True)
        assert AssertionError
        
    def test_edit_review(self):
        test = self.app.post('/user/review/1/edit/newcontent',data=dict(username='user'), follow_redirects=True)
        assert AssertionError

    def test_edit_event_name(self):
        test = self.app.post('/user/event/1/edit/name/newname',data=dict(username='user'), follow_redirects=True)
        assert AssertionError
        
    def test_event_manager(self):
        test = self.app.post('/user/event/1/edit/manager/1',data=dict(username='user'), follow_redirects=True)
        assert AssertionError
        
    def test_event_type(self):
        test = self.app.post('/user/event/1/edit/type/newtype',data=dict(username='user'), follow_redirects=True)
        assert AssertionError
        
    def test_edit_event_due(self):
        test = self.app.post('/user/event/1/edit/due/newdate',data=dict(username='user'), follow_redirects=True)
        assert AssertionError
        
    def test_del_event(self):
        test = self.app.post('/user/events/delete/1',data=dict(username='user'), follow_redirects=True)
        assert AssertionError
        
    def test_del_service(self):
        test = self.app.post('/user/services/delete/1',data=dict(username='user'), follow_redirects=True)
        assert AssertionError
        
    def test_del_user(self):
        test = self.app.post('/user/user/delete/1',data=dict(username='user'), follow_redirects=True)
        assert AssertionError
        
    def test_del_review(self):
        test = self.app.post('/user/review/delete/1',data=dict(username='user'), follow_redirects=True)
        assert AssertionError
        
    def test_del_package(self):
        test = self.app.post('/user/package/delete/1',data=dict(username='user'), follow_redirects=True)
        assert AssertionError

    def test_del_canvass(self):
        test = self.app.post('/user/canvass/1/delete/1',data=dict(username='user'), follow_redirects=True)
        assert AssertionError
        
    def test_del_hire(self):
        test = self.app.post('/user/hire/1/delete/1',data=dict(username='user'), follow_redirects=True)
        assert AssertionError
        
    def test_search(self):
        test = self.app.post('/search',data=dict(username='user'), follow_redirects=True)
        assert AssertionError
