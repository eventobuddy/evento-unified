// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'ngCookies', 'starter.controllers', 'starter.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'AppCtrl',
    onEnter: function($state, Auth){
        if(!Auth.isLoggedIn()){
           $state.go('login');
        }
    }
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html'
      }
    }
  })

  .state('login', {
    url: "/login",
    templateUrl: "templates/login.html",
    controller: 'AppCtrl'
  })

  .state('register', {
    url: "/register",
    templateUrl: "templates/register.html",
    controller: 'AppCtrl'
  })

  .state('app.selfportfolio', {
    url: "/portfolio",
    views: {
      'menuContent': {
        templateUrl: 'templates/portfolio.html',
        controller: 'PortfolioCtrl'
      }
    }
  })

  .state('app.portfolio', {
    url: "/portfolio/:username",
    views: {
      'menuContent': {
        templateUrl: 'templates/portfolio.html',
        controller: 'PortfolioCtrl'
      }
    }
  })

  .state('app.services', {
    url: "/services",
    views: {
      'menuContent': {
        templateUrl: 'templates/services.html',
        controller: 'ServicesCtrl'
      }
    }
  })

  .state('app.addservice', {
    url: "/services/add",
    views: {
      'menuContent': {
        templateUrl: 'templates/addservice.html',
        controller: 'ServicesCtrl'
      }
    }
  })

  .state('app.viewservice', {
    url: "/services/:serviceId",
    views: {
      'menuContent': {
        templateUrl: 'templates/viewservice.html',
        controller: 'ServiceCtrl'
      }
    }
  })

  .state('app.addpackage', {
    url: "/services/:serviceId/package/add",
    views: {
      'menuContent': {
        templateUrl: 'templates/addpackage.html',
        controller: 'PackagesCtrl'
      }
    }
  })

  .state('app.editpackage', {
    url: "/services/:serviceId/package/edit/:packageId",
    views: {
      'menuContent': {
        templateUrl: 'templates/editpackage.html',
        controller: 'PackageCtrl'
      }
    }
  })

  .state('app.messages', {
    url: "/message/:receiver",
    views: {
      'menuContent': {
        templateUrl: 'templates/messages.html',
        controller: 'MessagesCtrl'
      }
    }
  })

  .state('app.inbox', {
    url: "/inbox",
    views: {
      'menuContent': {
        templateUrl: 'templates/inbox.html',
        controller: 'MessagesCtrl'
      }
    }
  })

  .state('app.home', {
    url: "/home",
    views: {
        'menuContent': {
          templateUrl: 'templates/home.html'
        }
      }
  })

  .state('app.events', {
    url: "/events",
    views: {
      'menuContent': {
        templateUrl: 'templates/events.html',
        controller: 'EventsCtrl'
      }
    }
  })

  .state('app.addevents', {
    url: "/events/add",
    views: {
      'menuContent': {
        templateUrl: 'templates/addevent.html',
        controller: 'EventsCtrl'
      }
    }
  })

  .state('app.hire', {
    url: "/canvass/hire/:eventId/:serviceId",
    views: {
      'menuContent': {
        controller: "HireCtrl"
      }
    }
  })

  .state('app.canvass', {
    url: "/events/canvass/:eventId",
    views: {
      'menuContent': {
        templateUrl: 'templates/eventcanvass.html',
        controller: 'EventCtrl'
      }
    }
  })

  .state('app.canvassorganizers', {
    url: "/events/canvass/:eventId/services",
    views: {
      'menuContent': {
        templateUrl: 'templates/canvassservices.html',
        controller: 'EventServices'
      }
    }
  })

  .state('app.canvasspackages', {
    url: "/events/canvass/:eventId/services/:serviceId",
    views: {
      'menuContent': {
        templateUrl: 'templates/portfolioCanvassing.html',
        controller: 'EventServicesPackages'
      }
    }
  })

  .state('app.addtocanvass', {
    url: "/events/canvass/:eventId/services/:serviceId/:packageId",
    views: {
      'menuContent': {
        controller: 'EventServicesPackagesCanvass'
      }
    }
  })

  .state('app.browse', {
      url: '/browse',
      views: {
        'menuContent': {
          templateUrl: 'templates/browse.html'
        }
      }
    })
    .state('app.playlists', {
      url: '/playlists',
      views: {
        'menuContent': {
          templateUrl: 'templates/playlists.html',
          controller: 'PlaylistsCtrl'
        }
      }
    })

  .state('app.single', {
    url: '/playlists/:playlistId',
    views: {
      'menuContent': {
        templateUrl: 'templates/playlist.html',
        controller: 'PlaylistCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('login');
});
