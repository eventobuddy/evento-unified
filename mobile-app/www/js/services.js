angular.module('starter.services', ['ngCookies'])
.factory('Auth', function ($cookieStore) {
   var _user = $cookieStore.get('starter.user');
   var _id = $cookieStore.get('starter.user_id');

   var setId = function(id) {
      _id = id;
      $cookieStore.put('starter.user_id', _id);
   }

   var setUser = function (user) {
      _user = user;
      $cookieStore.put('starter.user', _user);
   }
 
   return {
      setId: setId,
      setUser: setUser,
      isLoggedIn: function () {
         return _user ? true : false;
      },
      getId: function () {
         return _id;
      },
      getUser: function () {
         return _user;
      },
      logout: function () {
         $cookieStore.remove('starter.user');
         $cookieStore.remove('starter.user_id');
         _user = null;
      }
   }
});