angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $state, Auth, $http, $timeout) {
  $scope.loginData = {}; 
  $scope.regData = {};
  $scope.usernameTest = Auth.getUser();

  $scope.logout = function() {
  	Auth.logout();
  	$state.go("login");
  };


   // Perform the login action when the user submits the login form
   $scope.doLogin = function() {

   	if(!angular.isDefined($scope.loginData.username) || !angular.isDefined($scope.loginData.password) || $scope.loginData.username.trim() == "" || $scope.loginData.password.trim() == ""){
   		alert("Enter both user name and password");
   		return;
   	} 

   	var data = {
   		username: $scope.loginData.username,
   		password: $scope.loginData.password
   	};

   	var config = {
   		headers : {
   			'Content-Type': 'application/json'
   		}
   	}

    

   	$http.post('http://eventoserver-ecinauce.rhcloud.com/login', data, config)
   	.success(function (response, status, headers, config) {
      var redirect = function () {
        Auth.setId(response.id);
        Auth.setUser($scope.loginData.username);
        $state.go("app.home");
      };


      $timeout(redirect, 5000);
    })
   	.error(function (data, status, header, config) {
   		alert(status);
   	});
   };

   $scope.doRegister = function() {
   	if(!angular.isDefined($scope.regData.username) || !angular.isDefined($scope.regData.password) || !angular.isDefined($scope.regData.confirmPassword) || !angular.isDefined($scope.regData.email) || !angular.isDefined($scope.regData.contact) || $scope.regData.username.trim() == "" || $scope.regData.password.trim() == "" || $scope.regData.confirmPassword.trim() == "" || $scope.regData.email.trim() == "" || $scope.regData.contact.trim() == ""){
   		alert("All fields are required.");
   		return;
   	}

   	if($scope.regData.confirmPassword.trim() != $scope.regData.password.trim()){
   			alert("Passwords do not match.");
   			return;
   	}

	var data = {
   		username: $scope.regData.username,
   		password: $scope.regData.password,
   		email: $scope.regData.email,
   		contact_number: $scope.regData.contact
   	};

   	var config = {
   		headers : {
   			'Content-Type': 'application/json'
   		}
   	}

   	$http.post('http://eventoserver-ecinauce.rhcloud.com/register', data, config)
   	.success(function (data, status, headers, config) {
      if (data.status[0][0] == "OK") {
					$state.go("login");                	
                }
            })
   	.error(function (data, status, header, config) {
   		alert("User has existed");
   	});
   };
})

.controller('PortfolioCtrl', function($scope, Auth, $http, $state, $stateParams) {
  if (angular.isDefined($stateParams.username)) {
    $scope.profileUsername = $stateParams.username;  
  }
  else {
    $scope.profileUsername = Auth.getUser();
  }
  $scope.userID = Auth.getId();
  $scope.userPortfolio = {};

  $http.get('http://eventoserver-ecinauce.rhcloud.com/' + $scope.profileUsername)
    .success(function (data, status, headers, config) {
        $scope.userPortfolio = data;
        $scope.ownerID = data.id;
        $scope.events = data.events;
        $scope.services = data.services;
    })
    .error(function (data, status, header, config) {
      $scope.userPortfolio = data;
    });


})

.controller('ServicesCtrl', function($scope, Auth, $http, $interval, $state) {
	var username = Auth.getUser();
  $scope.searchService = '';
	//$scope.services = {};
	$scope.addServiceData = {};

  var getServices = function () {
    $http.get('http://eventoserver-ecinauce.rhcloud.com/' + username + '/services')
    .success(function (data, status, headers, config) {
        $scope.services = data.services;
    })
    .error(function (data, status, header, config) {
      $scope.services = data;
    });
  };

	$interval(getServices, 2000);
	
	$scope.doAddService = function() {
		if(!angular.isDefined($scope.addServiceData.serviceName) || !angular.isDefined($scope.addServiceData.serviceType) || $scope.addServiceData.serviceName.trim() == "" || $scope.addServiceData.serviceType.trim() == ""){
   		alert("All fields are required.");
   		return;
   	}

   	$http.get('http://eventoserver-ecinauce.rhcloud.com/' + username + '/register/' + $scope.addServiceData.serviceName.trim() + '/' + $scope.addServiceData.serviceType.trim())
   	.success(function (data, status, headers, config) {
        $state.go("app.services");
    })
   	.error(function (data, status, header, config) {
   		$scope.services = 'CHECK NETWORK SETTINGS';
   	})

   	};
})

.controller('ServiceCtrl', function($scope, Auth, $http, $interval, $state, $stateParams) {
    $scope.selectedService = {};
    var username = Auth.getUser();
    $scope.userID = Auth.getId();

    var getServiceDetail = function () {
      $http.get('http://eventoserver-ecinauce.rhcloud.com/' + username + '/services/' + $stateParams.serviceId)
      .success(function (data, status, headers, config) {
        $scope.selectedService = data;
        $scope.serviceName = data.service[0].name;
        $scope.serviceType = data.service[0].type;
        $scope.ownerId = data.service[0].owner;
        $scope.packages = data.service[0].packages;
        //alert($scope.ownerId + ' ' + $scope.userID);
      })
      .error(function (data, status, header, config) {
        $scope.services = data;
      });
    };

    $interval(getServiceDetail, 4000);


})

.controller('MessagesCtrl', function($scope, Auth, $http, $interval, $state, $stateParams) {
	var username = Auth.getUser();
  $scope.addMessageData = {};

	$http.get('http://eventoserver-ecinauce.rhcloud.com/' + username + '/message')
   	.success(function (data, status, headers, config) {
        $scope.messages = data.messages;
    })
   	.error(function (data, status, header, config) {
   		$scope.messages = 'PESTE';
   	});

    var refreshMessage = function() {
      $http.get('http://eventoserver-ecinauce.rhcloud.com/' + username + '/message')
    .success(function (data, status, headers, config) {
        $scope.selectedmessages = data.messages;
    })
    .error(function (data, status, header, config) {
      $scope.selectedmessages = 'PESTE';
    });
    }

    $interval(refreshMessage, 3000);

  $scope.doSendMessage = function() {
    $http.get('http://eventoserver-ecinauce.rhcloud.com/' + username + '/message/' + $scope.addMessageData.recipient + '/' + $scope.addMessageData.messageContent)
    .success(function (data, status, headers, config) {

    })
    .error(function (data, status, header, config) {
      $scope.selectedmessages = 'PESTE';
    });
  };
})

.controller('PackagesCtrl', function($scope, Auth, $http, $state, $stateParams) {
  $scope.addPackageData = {};
  var username = Auth.getUser();

  $scope.doAddPackage = function() {
    $http.get('http://eventoserver-ecinauce.rhcloud.com/' + username + '/services/' + $stateParams.serviceId + '/addPackage/' + $scope.addPackageData.packageName + '/' + $scope.addPackageData.price)
    .success(function (data, status, headers, config) {
      $state.go('app.viewservice',{serviceId:$stateParams.serviceId});
    })
    .error(function (data, status, header, config) {
      $scope.services = data;
    });
  };
})

.controller('PackageCtrl', function($scope, Auth, $http, $state, $stateParams, $q){
  $scope.selectedPackage = {};
  $scope.editPackageData = {};

  var username = Auth.getUser();
  $scope.userID = Auth.getId();

  var oldPackageName;
  var oldPrice;

  $http.get('http://eventoserver-ecinauce.rhcloud.com/' + username + '/services/' + $stateParams.serviceId + '/packages/' + $stateParams.packageId)
    .success(function (data, status, headers, config) {
      $scope.selectedPackage = data.packages;
      $scope.editPackageData.packageName = data.packages.package;
      $scope.editPackageData.price = data.packages.price;
      oldPackageName = data.packages.package;
      oldPrice = data.packages.price;
    })
    .error(function (data, status, header, config) {
      $scope.services = data;
    });

  $scope.doEditPackage = function() {
    if ((oldPackageName != $scope.editPackageData.packageName) || (oldPrice != $scope.editPackageData.price)) {
      $scope.pkgName = $http.get('http://eventoserver-ecinauce.rhcloud.com/' + username + '/services/' + $stateParams.serviceId + '/packages/' + $scope.selectedPackage.id + '/edit/package/' + $scope.editPackageData.packageName, {cache: false});
      $scope.pkgPrice = $http.get('http://eventoserver-ecinauce.rhcloud.com/' + username + '/services/' + $stateParams.serviceId + '/packages/' + $scope.selectedPackage.id + '/edit/price/' + $scope.editPackageData.price, {'cache': false});

      $q.all([$scope.pkgName, $scope.pkgPrice]).then(function(val) {
        alert(val);
      });
    }
  };
})

.controller('EventsCtrl', function($scope, Auth, $interval, $http, $state) {
  $scope.searchEvent = '';
  $scope.addEventData = {};
  $scope.events = {};
  var username = Auth.getUser();
/*
  var getEvents = function () {
    $http.get('http://eventoserver-ecinauce.rhcloud.com/' + username + '/events')
    .success(function (data, statusCode, headers, config) {
        $scope.events = data.status;
    })
    .error(function (data, status, header, config) {
      $scope.events = data;
    });
  };

  $interval(getEvents, 1000);
*/
  $http.get('http://eventoserver-ecinauce.rhcloud.com/' + username + '/events')
    .success(function (data, statusCode, headers, config) {
        $scope.events = data.status;
    })
    .error(function (data, status, header, config) {
      $scope.events = data;
    });

  $scope.doAddEvent = function() {
    $http.get('http://eventoserver-ecinauce.rhcloud.com/' + username + '/add/' + $scope.addEventData.eventName)
    .success(function (data, status, headers, config) {
      $state.go('app.events');
    })
    .error(function (data, status, header, config) {
      $scope.services = data;
    });
  }
})

.controller('EventCtrl', function($scope, Auth, $http, $interval, $state, $stateParams) {
  var username = Auth.getUser();
  $scope.eventID = $stateParams.eventId;
/*
  var getEventCanvass = function () {
    $http.get('http://eventoserver-ecinauce.rhcloud.com/' + username + '/canvass/' + $stateParams.eventId)
    .success(function (data, status, headers, config) {
        $scope.selectedEvent = data.canvass[0][0];
    })
    .error(function (data, status, header, config) {
      $scope.selectedEvent = 'PESTE';
    });
  };

  $interval(getEventCanvass, 4000);
  */

    $http.get('http://eventoserver-ecinauce.rhcloud.com/' + username + '/canvass/' + $stateParams.eventId)
    .success(function (data, status, headers, config) {
        $scope.selectedEvent = data.canvass[0];
    })
    .error(function (data, status, header, config) {
      $scope.selectedEvent = 'DAMMIT, no connection';
    });  
})


.controller('HireCtrl', function($state, $scope, Auth, $http, $state, $stateParams) {
  $scope.pkgName = $http.get('http://eventoserver-ecinauce.rhcloud.com/' + username + '/canvass/' + $stateParams.eventId + '/delete/' + $stateParams.serviceId, {cache: false});
  $scope.pkgPrice = $http.get('http://eventoserver-ecinauce.rhcloud.com/' + username + '/hire/' + stateParams.eventId + '/' + $stateParams.serviceId, {'cache': false});

  $q.all([$scope.pkgName, $scope.pkgPrice]).then(function(val) {
        $state.go('app.canvass', {"eventId": $stateParams.eventId});
    });
  })

.controller('EventServices', function($scope, Auth, $http, $interval, $state, $stateParams) {
  var username = Auth.getUser();
  var searchServices = '';
  $scope.eventId = $stateParams.eventId;

  var getProviders = function () {
    $http.get('http://eventoserver-ecinauce.rhcloud.com/search')
    .success(function (data, status, headers, config) {
        $scope.providers = data;
        $scope.essentials = data.essentials;
        $scope.prices = data.prices;
        $scope.services = data.services;
        $scope.users = data.users;
    })
    .error(function (data, status, header, config) {
      $scope.providers = 'PESTE';
    });


  };

  $interval(getProviders, 5000);
})

.controller('EventServicesPackages', function($scope, Auth, $http, $interval, $state, $stateParams) {
  var username = Auth.getUser();
  var searchPackages = '';
  var eventID = $stateParams.eventId;
  var serviceID = $stateParams.serviceId;
  $scope.event = eventID;
  $scope.service = serviceID;

  $http.get('http://eventoserver-ecinauce.rhcloud.com/' + username + '/services/' + $stateParams.serviceId + '/packages')
    .success(function (data, status, headers, config) {
        $scope.packages = data.packages;
    })
    .error(function (data, status, header, config) {
      $scope.packages = 'PESTE';
    });

})

.controller('EventServicesPackagesCanvass', function($scope, Auth, $http, $interval, $state, $stateParams) {
  var username = Auth.getUser();
  var searchPackages = '';
  var eventID = $stateParams.eventId;
  var serviceID = $stateParams.serviceId;
  var packageID = $stateParams.packageId;

  $http.get('http://eventoserver-ecinauce.rhcloud.com/' + username + '/canvass/' + eventID + '/' + serviceID +'/' + packageID)
    .success(function (data, status, headers, config) {
        $state.go("app.canvass", {"eventId": eventID});
    })
    .error(function (data, status, header, config) {
      $scope.packages = 'PESTE';
    });

})

.controller('PlaylistsCtrl', function($scope) {
	$scope.playlists = [
	{ title: 'Reggae', id: 1 },
	{ title: 'Chill', id: 2 },
	{ title: 'Dubstep', id: 3 },
	{ title: 'Indie', id: 4 },
	{ title: 'Rap', id: 5 },
	{ title: 'Cowbell', id: 6 }
	];
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
});